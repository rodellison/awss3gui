# README #

AWSS3GUI - Is a simple JavaFX utility for allowing users to set AWS AccessID, Secret key, Bucket, and File Prefix values, (proxy values if necessary)  such to interact with a remote AWS S3 storage.

Allows for Object Put, List Objects, Object Delete, and Set Object View (Read or No Access) Permissions. 

### Setting Defaults ###
* Create a config.properties file, and place it in the same directory as your compiled JAR
* Include the following fields:

```
#!java

#AWS Settings
AWSBucket=<The target AWS Bucket>
AWSPrefix=<The target folder in the AWS Bucket, e.g. /Videos/someDir/>
AWSAccessID=<Your AWS Access ID>
AWSSecret=<Your AWS Secret>
#Proxy Settings
ProxyURL=<no HTTP or HTTPS, only the name for your proxy server>
ProxyPort=<your Proxy port>
ProxyUser=<your userid, if your proxy needs it..>
ProxyPass=<your password, if your proxy needs it..>

```


### Who do I talk to? ###

* Rod.r.ellison@gmail.com