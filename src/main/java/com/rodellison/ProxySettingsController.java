package com.rodellison;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by rrellis on 5/11/16.
 */
public class ProxySettingsController {

    //Proxy Settings fields
    public TextField txtProxyURL;
    public TextField txtProxyPort;
    public TextField txtProxyUser;
    public TextField txtProxyPass;

    public Button btnProxySettingsCancel;
    public Button btnProxySettingsSave;

    Stage dialogStage;
    FXMLLoader loader;
    ProxySettingsController myController;

    public void showSettings () {

        try {

            if (null == dialogStage) {

                loader = new FXMLLoader();
                loader.setLocation(AWSSettingsController.class.getResource("ProxySettings.fxml"));

                AnchorPane page = loader.load();
                // Create the dialog Stage.
                dialogStage = new Stage();
                dialogStage.setTitle("Proxy Settings");
                dialogStage.initModality(Modality.APPLICATION_MODAL);
                Scene scene = new Scene(page);
                dialogStage.setScene(scene);
            }

            //Previously presented, and closed.. don't recreate, just reshow.
            myController = loader.getController();
            myController.txtProxyURL.setText(ProxyInfo.getStrProxyURL());
            myController.txtProxyPort.setText(ProxyInfo.getStrProxyPort());
            myController.txtProxyUser.setText(ProxyInfo.getStrProxyUser());
            myController.txtProxyPass.setText(ProxyInfo.getStrProxyPass());

            dialogStage.showAndWait();


        } catch (Exception ex) {

            System.out.println("Exception caught in ProxySettingsController:showSettings: " + ex.getMessage());

        }

    }
    public void SettingsButtonHandler (ActionEvent myActionEvent) {
        String strButtonItem = ((Button) myActionEvent.getSource()).getId();

        switch (strButtonItem) {
            case "btnProxySettingsSave":
                doProxySettingsSaveData();
                break;
            case "btnProxySettingsCancel":
                doProxySettingsCancel();
                break;

        }
    }

    public void doProxySettingsSaveData () {
        // get a handle to the stage
        Stage stage = (Stage) btnProxySettingsSave.getScene().getWindow();
        // do what you have to do
        ProxyInfo.setStrProxyURL(txtProxyURL.getText());
        ProxyInfo.setStrProxyPort(txtProxyPort.getText());
        ProxyInfo.setStrProxyUser(txtProxyUser.getText());
        ProxyInfo.setStrProxyPass(txtProxyPass.getText());
        //stage.close is the same as hide();
        stage.close();


    }
    public void doProxySettingsCancel () {

        // get a handle to the stage
        Stage stage = (Stage) btnProxySettingsCancel.getScene().getWindow();
        //stage.close is the same as hide();
        stage.close();
    }




}
