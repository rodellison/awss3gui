package com.rodellison;

import com.google.common.collect.Multimap;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;


public class AWSS3Gui extends Application {

    //Main AWSS3Gui Window
    public   Label lblStatus;
    public   TextArea txtArea;

    private  AWSSettingsController myAWSSettingsController;
    private  ProxySettingsController myProxySettingsController;
    private  AWSPermissionsListController myAWSPermissionsListController;

    private  AWSCommands myAWSCommands;
    private Stage myMainStage;
    private String strFileToUpload = "";


    @Override
    public void start(Stage primaryStage) throws Exception{

         Parent root = FXMLLoader.load(getClass().getResource("AWSS3GuiForm.fxml"));
        myMainStage = primaryStage;
        myMainStage.setTitle("AWS S3 GUI");

        myMainStage.setScene(new Scene(root));
        myMainStage.show();


        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = new FileInputStream("config.properties");

            // load a properties file
            prop.load(input);

            // get the property values and set them in the appropriate Info file.

            AWSInfo.setStrAWSBucket(prop.getProperty("AWSBucket"));
            AWSInfo.setStrAWSPrefix(prop.getProperty("AWSPrefix"));
            AWSInfo.setStrAWSAccessID(prop.getProperty("AWSAccessID"));
            AWSInfo.setStrAWSSecret(prop.getProperty("AWSSecret"));
            AWSInfo.setStrAWSKMSEncryptionKeyId(prop.getProperty("AWSKMSEncryptionKeyID"));


            ProxyInfo.setStrProxyURL(prop.getProperty("ProxyURL"));
            ProxyInfo.setStrProxyPort(prop.getProperty("ProxyPort"));
            ProxyInfo.setStrProxyUser(prop.getProperty("ProxyUser"));
            ProxyInfo.setStrProxyPass(prop.getProperty("ProxyPass"));

        } catch (IOException ex) {

            if (ex.getMessage().contains("No such file") || ex.getMessage().contains("cannot find the file")) {
                System.out.println("config.properties was not found or did not have all expected fields. " +
                        "This isn't an issue, the user will just have to fill stuff out manually..");

                AWSInfo.setStrAWSBucket("");
                AWSInfo.setStrAWSPrefix("");
                AWSInfo.setStrAWSAccessID("");
                AWSInfo.setStrAWSSecret("");

                ProxyInfo.setStrProxyURL("");
                ProxyInfo.setStrProxyPort("");
                ProxyInfo.setStrProxyUser("");
                ProxyInfo.setStrProxyPass("");

            } else {
                ex.printStackTrace();
            }

        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        launch(args);

    }

    public void menuHandler (ActionEvent myActionEvent) {
        String strMenuItem = ((MenuItem) myActionEvent.getSource()).getText();

        switch (strMenuItem) {
            case "Exit":
                ExitApp();
                break;
            case "About":
                askAbout();
                break;
            case "AWS Settings":
                showAWSSettings();
                break;
            case "Proxy Settings":
                showProxySettings();
                break;
            case "List AWS Bucket Files":
                ListObjects();
                break;
            case "Select file for upload to AWS S3":
                SelectFileForUpload();
                break;
            case "Set Permissions on AWS S3 Objects":
                GetSetObjectPermissions();
                break;

        }

    }

    public void ShowListObjectsWithACLS () {

        //This function is invoked on completion of a TaskSucceed set in the AWSPermissionsListController.

        Multimap<String, Boolean> AWSS3ObjectsWithACL = myAWSCommands.getAWSS3ObjectsWithACLMap();

        try {
                if (myAWSPermissionsListController == null) {
                    myAWSPermissionsListController = new AWSPermissionsListController();
                }

            myAWSPermissionsListController.showSettings(AWSS3ObjectsWithACL, myAWSCommands);

            } catch (Exception ex) {

            }

    }

    public void GetSetObjectPermissions  () {

        lblStatus.setText("");
        txtArea.setText("");
        if (myAWSCommands == null) {
            myAWSCommands = new AWSCommands(this);
        }
         try {

            myAWSCommands.getObjectsAndPermissions();

        } catch (Exception myException) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("AWS Command Exception");
            alert.setHeaderText("AWS Command Exception");
            alert.setContentText(myException.getMessage());
            alert.showAndWait();
        }

    }

    public void SelectFileForUpload() {

        lblStatus.setText("");
        txtArea.setText("");
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select file for upload to AWS S3");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt"),
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"),
                new FileChooser.ExtensionFilter("Audio Files", "*.wav", "*.mp3", "*.aac"),
                new FileChooser.ExtensionFilter("Video Files", "*.mp4", "*.ogv", "*.ogg"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));

        File selectedFile = fileChooser.showOpenDialog(myMainStage);
        if (selectedFile != null) {
             strFileToUpload = selectedFile.getPath();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("Confirm file upload");
            alert.setContentText("Please confirm you want to upload file: " + strFileToUpload);
            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK){

               if (myAWSCommands == null) {
                    myAWSCommands = new AWSCommands(this);
                }
                lblStatus.setText(strFileToUpload + " to be uploaded");
                txtArea.setText("");
                try {
                    //User has confirmed they want to upload.. now call the function to start
                    alert = new Alert(Alert.AlertType.CONFIRMATION, "Do you want to encrypt file? - ONLY encrypt if this file isn't intended for public Cloudfront viewing. i.e. pure upload/download only.",
                            ButtonType.YES, ButtonType.NO);
                    alert.showAndWait();

                    boolean encryptFile = false;

                    if (alert.getResult() == ButtonType.YES) {
                        encryptFile  = true;
                    }
                    myAWSCommands.PutObjectToAWSBucket(strFileToUpload, encryptFile);


                } catch (Exception myException) {

                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("AWS Command Exception");
                    alert.setHeaderText("AWS Command Exception");
                    alert.setContentText(myException.getMessage());
                    alert.showAndWait();
                }
            }

        }
    }

    public void ListObjects  () {

        lblStatus.setText("");
        txtArea.setText("");
        if (myAWSCommands == null) {
            myAWSCommands = new AWSCommands(this);
        }

        try {
            myAWSCommands.ListObjectsFromAWSBucket();

        } catch (Exception myException) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("AWS Command Exception");
            alert.setHeaderText("AWS Command Exception");
            alert.setContentText(myException.getMessage());
            alert.showAndWait();
        }

    }

    public void askAbout  () {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About Dialog");
        alert.setHeaderText("About AWSS3Gui");
        alert.setContentText("created by Rod Ellison 5/1/2016");
        alert.showAndWait();

    }

    public void showAWSSettings() {

        lblStatus.setText("");
        txtArea.setText("");
        try {

            if (myAWSSettingsController == null) {
                myAWSSettingsController = new AWSSettingsController();
            }
            myAWSSettingsController.showSettings();

        } catch (Exception ex) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("AWSGui Exception");
            alert.setHeaderText("AWS Exception");
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
        }

    }
    public void showProxySettings() {

        lblStatus.setText("");
        txtArea.setText("");
        try {

            if (myProxySettingsController == null) {
                myProxySettingsController = new ProxySettingsController();
            }
            myProxySettingsController.showSettings();


        } catch (Exception ex) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("AWSGui Exception");
            alert.setHeaderText("AWS Exception");
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
        }
    }

    public void ExitApp() {

        Platform.exit();
    }
}
