package com.rodellison;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.amazonaws.*;
import com.amazonaws.auth.BasicAWSCredentials;


import com.amazonaws.http.AmazonHttpClient;

import com.amazonaws.regions.*;
import com.amazonaws.regions.Region;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;

import com.amazonaws.event.ProgressListener;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import javafx.animation.AnimationTimer;
import javafx.concurrent.Task;
import javafx.application.Platform;
import javafx.scene.control.TextArea;


import static java.lang.Thread.sleep;

/**
 * Created by rrellis on 5/12/16.
 */
class AWSCommands {

    public AWSS3Gui myMainFormController;
    private static long totalBytesTransferred = 0;

    //AWS Settings for Connection - Credentials, etc.
    private static BasicAWSCredentials awsCreds;
    private static TransferManager tx;
    private static AmazonS3 s3client;

    private static Multimap<String, Boolean> AWSS3ObjectsWithACL;

    public Multimap<String, Boolean> getAWSS3ObjectsWithACLMap () {
        return AWSS3ObjectsWithACL;
    }

    public AWSCommands(AWSS3Gui myController) {
        //This is needed for the encrypted uploads, and allowing them to be readable after they are in S3.
        System.setProperty(SDKGlobalConfiguration.ENFORCE_S3_SIGV4_SYSTEM_PROPERTY, "true");

        myMainFormController = myController;

        awsCreds = new BasicAWSCredentials(AWSInfo.getStrAWSAccessID(), AWSInfo.getStrAWSSecret());
        AWSS3ObjectsWithACL = ArrayListMultimap.create();
    }

    //This utility class creates a message pump to allow messages to be sent to the text area (in APPEND mode), at a controlled rate.
    //Task's (updateMessage) can throttle and DROP messages if sent to fast.. This class ensures all messages sent.
    //Animation timer ensures no more than one message per FRAME is send...
    public class MessageConsumer extends AnimationTimer {
        private final BlockingQueue<String> messageQueue ;
        private final TextArea textArea ;

        public MessageConsumer(BlockingQueue<String> messageQueue, TextArea textArea) {
            this.messageQueue = messageQueue ;
            this.textArea = textArea ;
        }
        @Override
        public void handle(long now) {
            if (messageQueue.size() > 0) {
                List<String> messages = new ArrayList<>();
                messageQueue.drainTo(messages, 5);
                messages.forEach(msg -> textArea.appendText(msg));
            }
        }
    }

    private AmazonS3 getMyS3client() {


        if (!ProxyInfo.getStrProxyURL().trim().isEmpty() || !ProxyInfo.getStrProxyPort().trim().isEmpty()) {
            // Proxy Logic
         ClientConfiguration clientConfig = new ClientConfiguration();
         clientConfig.setProtocol(Protocol.HTTPS);
         clientConfig.setSignerOverride("AWSS3V4SignerType");


            clientConfig.setProxyHost(ProxyInfo.getStrProxyURL());
            clientConfig.setProxyPort(Integer.parseInt(ProxyInfo.getStrProxyPort()));
            clientConfig.setProxyUsername(ProxyInfo.getStrProxyUser());
            clientConfig.setProxyPassword(ProxyInfo.getStrProxyPass());
   //         clientConfig.setConnectionTimeout(10000);

            return new AmazonS3Client(awsCreds, clientConfig)
                    .withRegion(Region.getRegion(Regions.US_EAST_1));

        } else {

            return new AmazonS3Client(awsCreds)
                    .withRegion(Region.getRegion(Regions.US_EAST_1));

  //          return new AmazonS3Client(awsCreds)
  //                  .withRegion(Region.getRegion(Regions.US_EAST_1));
        }


    }

    public void setSpecificObjectPermission(String strObjectKey, boolean VisibleToAll) {
        //Create and Start a blocking queue to put messages into, that will drain later at a rate no faster than PER-FRAME.
        //Using this so we can have the TextArea APPEND messages that could get long..
        BlockingQueue<String> messageQueue = new LinkedBlockingQueue<>();
        Platform.runLater(() -> new MessageConsumer(messageQueue, myMainFormController.txtArea).start());

        try {

            messageQueue.put("Creating AWS S3 Client.\n");

            s3client = getMyS3client();

            messageQueue.put("AWS S3 Client created successfully.\n");

            messageQueue.put("Getting existing ACL Grants for ObjectKey: " + strObjectKey + "\n");
            List<Grant> thisObjectACLGrants = s3client.getObjectAcl(AWSInfo.getStrAWSBucket(), strObjectKey).getGrantsAsList();

            AccessControlList acl = new AccessControlList();
            if (VisibleToAll == true) {
                messageQueue.put("Setting AWS S3 ACL of item: " + strObjectKey + " to be Visible.\n");
                s3client.setObjectAcl(AWSInfo.getStrAWSBucket(), strObjectKey, CannedAccessControlList.PublicRead);
            } else {
                messageQueue.put("Setting AWS S3 ACL of item: " + strObjectKey + " to be NO Access.\n");
                s3client.setObjectAcl(AWSInfo.getStrAWSBucket(), strObjectKey, CannedAccessControlList.Private);
            }
            SendMessageToMainGuiStatus("AWS S3 Object Permission change completed.");

        } catch (InterruptedException intExc) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    myMainFormController.txtArea.setText(intExc.getMessage());
                }
            });

        } catch (AmazonServiceException ase) {

            StringBuilder myLongString = new StringBuilder();
            myLongString.append("Caught an AmazonServiceException \n ");
            myLongString.append("Error Message:    " + ase.getMessage() + "\n");
            myLongString.append("AWS Error Code:   " + ase.getErrorCode() + "\n");
            myLongString.append("Error Type:       " + ase.getErrorType() + "\n");
            myLongString.append("Request ID:       " + ase.getRequestId() + "\n");
            myLongString.append("Service Name:     " + ase.getServiceName() + "\n");
            myLongString.append("Status Code:      " + ase.getStatusCode() + "\n");

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    myMainFormController.txtArea.setText(myLongString.toString());
                }
            });

        } catch (AmazonClientException ace) {

            StringBuilder myLongString = new StringBuilder();
            myLongString.append("Caught an AmazonClientException,the client encountered an " +
                    "internal error while trying to communicate with AWS S3");
            myLongString.append("Error Message: " + ace.getMessage());

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    myMainFormController.txtArea.setText(myLongString.toString());
                }
            });

        }
        return;

    }

    public void deleteObject(String strObjectKey) {
        //Create and Start a blocking queue to put messages into, that will drain later at a rate no faster than PER-FRAME.
        //Using this so we can have the TextArea APPEND messages that could get long..
        BlockingQueue<String> messageQueue = new LinkedBlockingQueue<>();
        Platform.runLater(() -> new MessageConsumer(messageQueue, myMainFormController.txtArea).start());

        try {

            messageQueue.put("Creating AWS S3 Client.\n");

            s3client = getMyS3client();

            messageQueue.put("AWS S3 Client created successfully.\n");

            messageQueue.put("Attempting delete for ObjectKey: " + strObjectKey + "\n");
            DeleteObjectRequest myDeleteRequest = new DeleteObjectRequest(AWSInfo.getStrAWSBucket(), strObjectKey);
            myDeleteRequest.setSdkClientExecutionTimeout(5000);

            s3client.deleteObject(myDeleteRequest);
            messageQueue.put("ObjectKey deleted successfully.\n");

            SendMessageToMainGuiStatus("AWS S3 Object delete completed.");

        } catch (InterruptedException intExc) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    myMainFormController.txtArea.setText(intExc.getMessage());
                }
            });

        } catch (AmazonServiceException ase) {

            StringBuilder myLongString = new StringBuilder();
            myLongString.append("Caught an AmazonServiceException \n ");
            myLongString.append("Error Message:    " + ase.getMessage() + "\n");
            myLongString.append("AWS Error Code:   " + ase.getErrorCode() + "\n");
            myLongString.append("Error Type:       " + ase.getErrorType() + "\n");
            myLongString.append("Request ID:       " + ase.getRequestId() + "\n");
            myLongString.append("Service Name:     " + ase.getServiceName() + "\n");
            myLongString.append("Status Code:      " + ase.getStatusCode() + "\n");

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    myMainFormController.txtArea.setText(myLongString.toString());
                }
            });

        } catch (AmazonClientException ace) {

            StringBuilder myLongString = new StringBuilder();
            myLongString.append("Caught an AmazonClientException,the client encountered an " +
                    "internal error while trying to communicate with AWS S3");
            myLongString.append("Error Message: " + ace.getMessage());

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    myMainFormController.txtArea.setText(myLongString.toString());
                }
            });

        }
        return;
    }

    public void getObjectsAndPermissions()  {

        //Create and Start a blocking queue to put messages into, that will drain later at a rate no faster than PER-FRAME.
        //Using this so we can have the TextArea APPEND messages that could get long..
        BlockingQueue<String> messageQueue = new LinkedBlockingQueue<>();
        Platform.runLater(() -> new MessageConsumer(messageQueue, myMainFormController.txtArea).start());

     //Create a task thread to run this longer running bit of code on..
        //Also used to allow txtArea updates on the main form.
        Task<Boolean> task = new Task<Boolean>() {
            public Boolean call() throws Exception {

                if (myMainFormController == null) {
                    messageQueue.put("The main app controller was not provided. Need it for writing to the Text Area of the main form.");
                    return false;
                }

                if (AWSInfo.getStrAWSAccessID() == "" || AWSInfo.getStrAWSSecret() == "") {
                    messageQueue.put("AWS Credentials were not provided. Check AWS Settings");
                    return false;
                }
                if (AWSInfo.getStrAWSBucket() == "" || AWSInfo.getStrAWSPrefix() == "") {
                    messageQueue.put("AWS Bucket information not provided. Check AWS Settings");
                    return false;
                }

                try {

                    messageQueue.put("Creating AWS S3 Client.\n");

                    s3client = getMyS3client();

                    messageQueue.put("AWS S3 Client created successfully.\n");
                    messageQueue.put("Creating Object List request.\n");

                    ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                            .withBucketName(AWSInfo.getStrAWSBucket())
                            .withPrefix(AWSInfo.getStrAWSPrefix())
                            .withSdkClientExecutionTimeout(10000);

                     messageQueue.put("Object List request created successfully.\n");

                    ObjectListing objectListing;
                    do {
                        messageQueue.put("Calling AWS S3 listObjects.\n");
                        objectListing = s3client.listObjects(listObjectsRequest);

                        //Make sure our Multimap is cleared first, before populating with new data.
                        AWSS3ObjectsWithACL.clear();
                        for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                            if (objectSummary.getSize() != 0) {

                                List<Grant>thisObjectACLGrants=s3client.getObjectAcl(objectSummary.getBucketName(),objectSummary.getKey()).getGrantsAsList();
                                boolean isVisibleToAllUsers = false;
                                for(Grant myGrant:thisObjectACLGrants){
                                    if (myGrant.getGrantee().toString().toLowerCase().contains("allusers")) {
                                        isVisibleToAllUsers = true;
                                    }
                                }
                                AWSS3ObjectsWithACL.put(objectSummary.getKey(), isVisibleToAllUsers);
                            }
                        }
                        listObjectsRequest.setMarker(objectListing.getNextMarker());
                    } while (objectListing.isTruncated());

                    SendMessageToMainGuiStatus("AWS S3 listObjects completed.");

                } catch (AmazonServiceException ase) {

                    StringBuilder myLongString = new StringBuilder();
                    myLongString.append("Caught an AmazonServiceException \n ");
                    myLongString.append("Error Message:    " + ase.getMessage() + "\n");
                    myLongString.append("AWS Error Code:   " + ase.getErrorCode() + "\n");
                    myLongString.append("Error Type:       " + ase.getErrorType() + "\n");
                    myLongString.append("Request ID:       " + ase.getRequestId() + "\n");
                    myLongString.append("Service Name:     " + ase.getServiceName() + "\n");
                    myLongString.append("Status Code:      " + ase.getStatusCode() + "\n");
                    messageQueue.put(myLongString.toString());
                    return false;

                } catch (AmazonClientException ace) {

                    StringBuilder myLongString = new StringBuilder();
                    myLongString.append("Caught an AmazonClientException,the client encountered an " +
                            "internal error while trying to communicate with AWS S3");
                    myLongString.append("Error Message: " + ace.getMessage());

                    messageQueue.put(myLongString.toString());
                    return false;

                }
                return true;
            }
        };
        //As soon as this long running task completes, notify the AWSS3Gui controller
        task.setOnSucceeded(e -> {
            if (task.getValue())
                myMainFormController.ShowListObjectsWithACLS();
            });
        new Thread(task).start();
    }

    public void ListObjectsFromAWSBucket()  {

        //Create and Start a blocking queue to put messages into, that will drain later at a rate no faster than PER-FRAME.
        //Using this so we can have the TextArea APPEND messages that could get long..
        BlockingQueue<String> messageQueue = new LinkedBlockingQueue<>();
        Platform.runLater(() -> new MessageConsumer(messageQueue, myMainFormController.txtArea).start());

        //Create a task thread to run this longer running bit of code on..
        //Also used to allow txtArea updates on the main form.
        Task<Void> task = new Task<Void>() {
            public Void call() throws Exception {

                if (myMainFormController == null) {
                    messageQueue.put("The main app controller was not provided. Need it for writing to the Text Area of the main form.");
                    return null;
                }

                if (AWSInfo.getStrAWSAccessID() == "" || AWSInfo.getStrAWSSecret() == "") {
                    messageQueue.put("AWS Credentials were not provided. Check AWS Settings");
                    return null;
                }
                if (AWSInfo.getStrAWSBucket() == "" || AWSInfo.getStrAWSPrefix() == "") {
                    messageQueue.put("AWS Bucket information not provided. Check AWS Settings");
                    return null;
                }

                try {

                    messageQueue.put("Creating AWS S3 Client.\n");

                    s3client = getMyS3client();

                    messageQueue.put("AWS S3 Client created successfully.\n");
                    messageQueue.put("Creating Object List request.\n");

                    ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                            .withBucketName(AWSInfo.getStrAWSBucket())
                            .withPrefix(AWSInfo.getStrAWSPrefix())
                            .withSdkClientExecutionTimeout(10000);

                    messageQueue.put("Object List request created successfully.\n");

                    ObjectListing objectListing;
                    do {
                        messageQueue.put("Calling AWS S3 listObjects.\n");
                        objectListing = s3client.listObjects(listObjectsRequest);

                        messageQueue.put("\nThe URLS to share for these objects:\n");

                        for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                            if (objectSummary.getSize() != 0) {
                                messageQueue.put("https://dxdfbjvc4p4rb.cloudfront.net/" + objectSummary.getKey() + "\n");
                            }
                        }
                        listObjectsRequest.setMarker(objectListing.getNextMarker());
                    } while (objectListing.isTruncated());

                    SendMessageToMainGuiStatus("AWS S3 listObjects completed.");

                } catch (AmazonServiceException ase) {

                    StringBuilder myLongString = new StringBuilder();
                    myLongString.append("Caught an AmazonServiceException \n ");
                    myLongString.append("Error Message:    " + ase.getMessage() + "\n");
                    myLongString.append("AWS Error Code:   " + ase.getErrorCode() + "\n");
                    myLongString.append("Error Type:       " + ase.getErrorType() + "\n");
                    myLongString.append("Request ID:       " + ase.getRequestId() + "\n");
                    myLongString.append("Service Name:     " + ase.getServiceName() + "\n");
                    myLongString.append("Status Code:      " + ase.getStatusCode() + "\n");
                    messageQueue.put(myLongString.toString());

                } catch (AmazonClientException ace) {

                    StringBuilder myLongString = new StringBuilder();
                    myLongString.append("Caught an AmazonClientException,the client encountered an " +
                            "internal error while trying to communicate with AWS S3");
                    myLongString.append("Error Message: " + ace.getMessage());

                    messageQueue.put(myLongString.toString());

                }
                return null;
            }
        };
        new Thread(task).start();

    }

    private void SendMessageToMainGuiStatus(String myMessage) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                myMainFormController.lblStatus.setText(myMessage);
            }
        });
    }

    public void PutObjectToAWSBucket(String strFileToUpload, boolean encryptFile)  {

        //Create and Start a blocking queue to put messages into, that will drain later at a rate no faster than PER-FRAME.
        //Using this so we can have the TextArea APPEND messages that could get long..
        BlockingQueue<String> messageQueue = new LinkedBlockingQueue<>();
        Platform.runLater(() -> new MessageConsumer(messageQueue, myMainFormController.txtArea).start());

        //Create a task thread to run this longer running bit of code on..
        //Also used to allow txtArea updates on the main form.
        Task<Void> task = new Task<Void>() {
            public Void call() throws Exception {

                File fileToUpload;
                String fileName;
                ProgressListener progressListener;

                if (myMainFormController == null) {
                    messageQueue.put("The main app controller was not provided. Need it for writing to the Text Area of the main form.");
                    return null;
                }
                fileToUpload = new File(strFileToUpload);
                fileName = fileToUpload.getName().replace(" ", "_");  //make sure no spaces in name, so that later - the ACL commands for setting and removing visibility work.

                if (fileToUpload.length() > 100000000) {
                    messageQueue.put("File too large, please reduce the size of your file. 100Meg max bytes" + "\n");
                    return null;
                }

                if (AWSInfo.getStrAWSAccessID() == "" || AWSInfo.getStrAWSSecret() == "") {
                    messageQueue.put("AWS Credentials were not provided. Check AWS Settings");
                    return null;
                }
                if (AWSInfo.getStrAWSBucket() == "" || AWSInfo.getStrAWSPrefix() == "") {
                    messageQueue.put("AWS Bucket information not provided. Check AWS Settings");
                    return null;
                }
                if (strFileToUpload.isEmpty()) {
                    messageQueue.put("A file to upload has not been selected. Please select File/Select file to Upload..");
                    return null;
                }

                //Get the mime type for the file being uploaded.
                String fileExtension = strFileToUpload.substring(strFileToUpload.lastIndexOf('.') + 1).trim();
                String myMimeType = AWSContentMimeTypes.getMatchMimeType(fileExtension.toLowerCase());

                try {

                    messageQueue.put("Creating AWS S3 Transfer Manager client.\n");

                    tx = new TransferManager(getMyS3client());

                    messageQueue.put("AWS S3 Transer Manager client created successfully\n");
                    messageQueue.put("Creating Object Content Metadata\n");

                    ObjectMetadata metadata = new ObjectMetadata();
                    metadata.setContentType(myMimeType);
                    metadata.setContentLength(fileToUpload.length());


                    messageQueue.put("Establishing AWS S3 put request object\n");

                    PutObjectRequest request;
                    request = new PutObjectRequest(AWSInfo.getStrAWSBucket(), AWSInfo.getStrAWSPrefix() + fileName, fileToUpload )
                            .withMetadata(metadata);
                    if (encryptFile) {
                         if (!AWSInfo.getStrAWSKMSEncryptionKeyId().isEmpty()) {
                             messageQueue.put("Put request upload will be Server side encrypted.\n");
                             request.setSSEAwsKeyManagementParams(new SSEAwsKeyManagementParams(AWSInfo.getStrAWSKMSEncryptionKeyId()));
                         }
                    }

                    messageQueue.put("Establishing Progress Listener to update byte counts for upload\n");
                    progressListener = new ProgressListener() {

                        public void progressChanged(ProgressEvent progressEvent) {

                            totalBytesTransferred += progressEvent.getBytesTransferred();

                            SendMessageToMainGuiStatus("Bytes transferred: " + totalBytesTransferred);

                            if (progressEvent.getEventType() == ProgressEventType.TRANSFER_COMPLETED_EVENT) {

                                SendMessageToMainGuiStatus("Bytes transferred: " + totalBytesTransferred + " - File upload completed successfully!");
                                tx.shutdownNow();
                                totalBytesTransferred = 0;
                            }
                        }

                    };

                    messageQueue.put("Starting upload!\n");
                    Upload upload = tx.upload(request);
                    upload.addProgressListener(progressListener);


                } catch (AmazonServiceException ase) {

                    StringBuilder myLongString = new StringBuilder();
                    myLongString.append("Caught an AmazonServiceException \n ");
                    myLongString.append("Error Message:    " + ase.getMessage() + "\n");
                    myLongString.append("AWS Error Code:   " + ase.getErrorCode() + "\n");
                    myLongString.append("Error Type:       " + ase.getErrorType() + "\n");
                    myLongString.append("Request ID:       " + ase.getRequestId() + "\n");
                    myLongString.append("Service Name:     " + ase.getServiceName() + "\n");
                    myLongString.append("Status Code:      " + ase.getStatusCode() + "\n");
                    messageQueue.put(myLongString.toString());

                } catch (AmazonClientException ace) {

                    StringBuilder myLongString = new StringBuilder();
                    myLongString.append("Caught an AmazonClientException,the client encountered an " +
                            "internal error while trying to communicate with AWS S3");
                    myLongString.append("Error Message: " + ace.getMessage());

                    messageQueue.put(myLongString.toString());

                }
                return null;
            }
        };

        new Thread(task).start();

    }

}
