package com.rodellison;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by rrellis on 5/11/16.
 */
public class AWSSettingsController {

    //AWS Settings fields
    public TextField txtBucket;
    public TextField txtPrefix;
    public TextField txtAccessID;
    public TextField txtSecret;
    public TextField txtKMSKeyID;

    public Button btnAWSSettingsCancel;
    public Button btnAWSSettingsSave;

    Stage dialogStage;
    FXMLLoader loader;
    AWSSettingsController myController;

    public void showSettings () {

        try {


            if (null == dialogStage) {

                loader = new FXMLLoader();
                loader.setLocation(AWSSettingsController.class.getResource("AWSSettings.fxml"));

                AnchorPane page = loader.load();
                // Create the dialog Stage.
                dialogStage = new Stage();
                dialogStage.setTitle("AWS Settings");
                dialogStage.initModality(Modality.APPLICATION_MODAL);
                Scene scene = new Scene(page);
                dialogStage.setScene(scene);
            }

            //Previously presented, and closed.. don't recreate, just reshow.
            myController = loader.getController();
            myController.txtBucket.setText(AWSInfo.getStrAWSBucket());
            myController.txtPrefix.setText(AWSInfo.getStrAWSPrefix());
            myController.txtAccessID.setText(AWSInfo.getStrAWSAccessID());
            myController.txtSecret.setText(AWSInfo.getStrAWSSecret());
            myController.txtKMSKeyID.setText(AWSInfo.getStrAWSKMSEncryptionKeyId());

            dialogStage.showAndWait();


        } catch (Exception ex) {

            System.out.println("Exception caught in AWSSettingsController:showSettings: " + ex.getMessage());

        }
    }

    public void SettingsButtonHandler (ActionEvent myActionEvent) {
        String strButtonItem = ((Button) myActionEvent.getSource()).getId();

        switch (strButtonItem) {
            case "btnAWSSettingsSave":
                doAWSSettingsSaveData();
                break;
            case "btnAWSSettingsCancel":
                doAWSSettingsCancel();
                break;

        }

    }

    public void doAWSSettingsSaveData () {
        // get a handle to the stage
        Stage stage = (Stage) btnAWSSettingsSave.getScene().getWindow();
        // do what you have to do
        AWSInfo.setStrAWSBucket(txtBucket.getText());
        AWSInfo.setStrAWSPrefix(txtPrefix.getText());
        AWSInfo.setStrAWSAccessID(txtAccessID.getText());
        AWSInfo.setStrAWSSecret(txtSecret.getText());
        AWSInfo.setStrAWSKMSEncryptionKeyId(txtKMSKeyID.getText());
        //stage.close is the same as hide();
        stage.close();


    }
    public void doAWSSettingsCancel () {

        // get a handle to the stage
        Stage stage = (Stage) btnAWSSettingsCancel.getScene().getWindow();

        //stage.close is the same as hide();
        stage.close();
    }




}
