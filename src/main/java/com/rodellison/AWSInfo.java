package com.rodellison;

/**
 * Created by rrellis on 5/12/16.
 */
public class AWSInfo {

    private static String strAWSBucket;
    private static String strAWSPrefix;
    private static String strAWSAccessID;
    private static String strAWSSecret;
    private static String strAWSKMSEncryptionKeyId;


    public static String getStrAWSKMSEncryptionKeyId() {
        return strAWSKMSEncryptionKeyId;
    }

    public static void setStrAWSKMSEncryptionKeyId(String strAWSKMSEncryptionKeyId) {
        AWSInfo.strAWSKMSEncryptionKeyId = strAWSKMSEncryptionKeyId;
    }

    public static String getStrAWSSecret() {
        return strAWSSecret;
    }

    public static void setStrAWSSecret(String strAWSSecret) {
        AWSInfo.strAWSSecret = strAWSSecret;
    }

    public static String getStrAWSBucket() {
        return strAWSBucket;
    }

    public static void setStrAWSBucket(String strAWSBucket) {
        AWSInfo.strAWSBucket = strAWSBucket;
    }

    public static String getStrAWSPrefix() {
        return strAWSPrefix;
    }

    public static void setStrAWSPrefix(String strAWSPrefix) {
        AWSInfo.strAWSPrefix = strAWSPrefix;
    }

    public static String getStrAWSAccessID() {
        return strAWSAccessID;
    }

    public static void setStrAWSAccessID(String strAWSAccessID) {
        AWSInfo.strAWSAccessID = strAWSAccessID;
    }



}
