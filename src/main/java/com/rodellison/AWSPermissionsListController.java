package com.rodellison;

import com.google.common.collect.Multimap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.*;

/**
 * Created by rrellis on 5/11/16.
 */
public class AWSPermissionsListController {

    //AWS Settings fields

    public ListView<String> myListView;
    public VBox myVBox;

    public Button btnAWSPermissionRead;
    public Button btnAWSPermissionNone;
    public Button btnClose;
    private static AWSCommands thisAWSCommands;

    private static Stage dialogStage;
    private static FXMLLoader loader;
    private static AWSPermissionsListController myController;
    private static Multimap<String, Boolean> myAWSS3ObjectsWithACL;
    private static ObservableList data;


    public void showSettings (Multimap<String, Boolean> AWSS3ObjectsWithACL, AWSCommands myAWSCommands) {

        myAWSS3ObjectsWithACL = AWSS3ObjectsWithACL;
        thisAWSCommands = myAWSCommands;
        data =  FXCollections.observableArrayList();
        for (Map.Entry<String, Boolean> collection : myAWSS3ObjectsWithACL.entries()) {
            data.add(collection.getKey() + "\tViewable: " + collection.getValue().toString());
            }

        try {

            if (null == dialogStage) {

                loader = new FXMLLoader();
                loader.setLocation(AWSPermissionsListController.class.getResource("AWSPermissionsList.fxml"));

                AnchorPane page = loader.load();
                // Create the dialog Stage.
                dialogStage = new Stage();
                dialogStage.setTitle("AWS Object Permissions");
                dialogStage.initModality(Modality.APPLICATION_MODAL);
                Scene scene = new Scene(page);
                dialogStage.setScene(scene);
            }

            //Previously presented, and closed.. don't recreate, just reshow.
            myController = loader.getController();

            //Make sure the ListView is cleared, in case this is a second or later presentation of the dialog
            myController.myListView.getItems().clear();

            //Load the items from the Observable list, into the Listview and assign a mousehandler to handle item clicking
            myController.myListView.setItems(data);

            dialogStage.showAndWait();


        } catch (Exception ex) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("AWS Permissions List Controller Exception");
            alert.setHeaderText("AWS Permissions Exception");
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
            System.out.println("Exception caught in AWSPermissionsListController:showSettings: " + ex.getMessage());

        }

    }


    public void SettingsButtonHandler (ActionEvent myActionEvent) {
        String strButtonItem = ((Button) myActionEvent.getSource()).getId();

        switch (strButtonItem) {
            case "btnAWSPermissionRead":
                doAWSPermissionSet(true);
                break;
            case "btnAWSPermissionNone":
                doAWSPermissionSet(false);
                break;
            case "btnAWSDeleteItem":
                doAWSDeleteItem();
                break;
            case "btnClose":
                doAWSClosePermissionWindow();
                break;

        }

    }

    public void doAWSDeleteItem () {

        int selectedIndex = myListView.getSelectionModel().getSelectedIndex();
        String selectedItem = myListView.getSelectionModel().getSelectedItem();
        String[] selectedItemFile ;

        try {
            if (selectedIndex >= 0 ) {
                selectedItemFile = selectedItem.split(" ");
                //Remove this portion that was visible in the ListView selected item.
                selectedItemFile[0] = selectedItemFile[0].replace("\tViewable:", "");
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Confirmation Dialog");
                alert.setHeaderText("Please confirm delete of ObjectKey: " + selectedItemFile[0]);
                alert.setContentText("Are you ok with this?");

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() != ButtonType.OK){
                    return;
                }

               thisAWSCommands.deleteObject(selectedItemFile[0]);
                if (!removeValue(myAWSS3ObjectsWithACL, selectedItemFile[0])) {
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("AWS Permissions List Dialog");
                    alert.setHeaderText("Object Delete request");
                    alert.setContentText("AWS Delete succeeded, but internal list was not updated. May need to close Permissions dialog and relaunch.");
                    alert.showAndWait();
                } else {

                    data =  FXCollections.observableArrayList();
                    for (Map.Entry<String, Boolean> collection : myAWSS3ObjectsWithACL.entries()) {
                        data.add(collection.getKey() + "\tViewable: " + collection.getValue().toString());
                    }
                    //Make sure the ListView is cleared, in case this is a second or later presentation of the dialog
                    myListView.getItems().clear();
                    //Load the items from the Observable list, into the Listview and assign a mousehandler to handle item clicking
                    myListView.setItems(data);
                }
            }
            else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("AWS Permissions List Dialog");
                alert.setHeaderText("Selection required");
                alert.setContentText("Please select an item from the list");
                alert.showAndWait();
            }
        } catch (Exception ex) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("AWS Command Exception");
            alert.setHeaderText("AWS Command Exception");
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
        }
    }

    public static <String,Boolean> boolean replaceValue(Multimap<String,Boolean> map, String key, Boolean oldValue, Boolean newValue) {
        if (map.remove(key, oldValue)) {
            map.put(key, newValue);
            return true;
        }
        return false;
    }
    public static <String,Boolean> boolean removeValue(Multimap<String,Boolean> map, String key) {
        Set<String> set = new HashSet<> ();
        set.add(key);
        if (map.keySet().removeAll(set)) {
            return true;
        }
        return false;
    }
     public void doAWSPermissionSet (boolean blnVisible) {

         int selectedIndex = myListView.getSelectionModel().getSelectedIndex();
         String selectedItem = myListView.getSelectionModel().getSelectedItem();
         String[] selectedItemFile ;
         boolean newVisibleSetting = blnVisible;
         boolean oldVisibleSetting = (blnVisible == true) ? false : true;

         try {
             if (selectedIndex >= 0 ) {
                 selectedItemFile = selectedItem.split(" ");
                 //Remove this portion that was visible in the ListView selected item.
                 selectedItemFile[0] = selectedItemFile[0].replace("\tViewable:", "");

                 Collection values = myAWSS3ObjectsWithACL.get(selectedItemFile[0]);
                 Iterator valueIterator = values.iterator();
                 while (valueIterator.hasNext()) {
                     boolean myValue = (boolean) valueIterator.next();
                     if (myValue != blnVisible) {
                         //OK, permission is different than previous, process change..
                         thisAWSCommands.setSpecificObjectPermission(selectedItemFile[0], blnVisible);
                         if (!replaceValue(myAWSS3ObjectsWithACL, selectedItemFile[0], oldVisibleSetting, newVisibleSetting)) {
                             Alert alert = new Alert(Alert.AlertType.INFORMATION);
                             alert.setTitle("AWS Permissions List Dialog");
                             alert.setHeaderText("Permissions Change request");
                             alert.setContentText("AWS Permission succeeded, but internal list was not updated. May need to close Permissions dialog and relaunch.");
                             alert.showAndWait();
                         } else {

                             data =  FXCollections.observableArrayList();
                             for (Map.Entry<String, Boolean> collection : myAWSS3ObjectsWithACL.entries()) {
                                 data.add(collection.getKey() + "\tViewable: " + collection.getValue().toString());
                             }
                             //Make sure the ListView is cleared, in case this is a second or later presentation of the dialog
                             myListView.getItems().clear();
                             //Load the items from the Observable list, into the Listview and assign a mousehandler to handle item clicking
                             myListView.setItems(data);
                         }
                         break;
                     } else {

                         Alert alert = new Alert(Alert.AlertType.INFORMATION);
                         alert.setTitle("AWS Permissions List Dialog");
                         alert.setHeaderText("Permissions Change request");
                         alert.setContentText("ObjectKey was found, but already matches the Visibility requested.. no changes needed.");
                         alert.showAndWait();
                         break;
                     }
                 }
             }
             else {
                 Alert alert = new Alert(Alert.AlertType.INFORMATION);
                 alert.setTitle("AWS Permissions List Dialog");
                 alert.setHeaderText("Selection required");
                 alert.setContentText("Please select an item from the list");
                 alert.showAndWait();
             }
         } catch (Exception ex) {
             Alert alert = new Alert(Alert.AlertType.INFORMATION);
             alert.setTitle("AWS Command Exception");
             alert.setHeaderText("AWS Command Exception");
             alert.setContentText(ex.getMessage());
             alert.showAndWait();
         }

    }

    public void doAWSClosePermissionWindow () {

        // get a handle to the stage
        Stage stage = (Stage) btnClose.getScene().getWindow();
         //stage.close is the same as hide();
        stage.close();
    }




}
