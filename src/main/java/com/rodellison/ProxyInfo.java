package com.rodellison;

/**
 * Created by rrellis on 5/12/16.
 */
public class ProxyInfo {

    private static String strProxyURL;
    private static String strProxyPort;
    private static String strProxyUser;
    private static String strProxyPass;


    public static String getStrProxyURL() {
        return strProxyURL;
    }

    public static void setStrProxyURL(String strProxyURL) {
        ProxyInfo.strProxyURL = strProxyURL;
    }

    public static String getStrProxyPort() {
        return strProxyPort;
    }

    public static void setStrProxyPort(String strProxyPort) {
        ProxyInfo.strProxyPort = strProxyPort;
    }

    public static String getStrProxyUser() {  return strProxyUser;    }

    public static void setStrProxyUser(String strProxyUser) {
        ProxyInfo.strProxyUser = strProxyUser;
    }

    public static String getStrProxyPass() {
        return strProxyPass;
    }

    public static void setStrProxyPass(String strProxyPass) {
        ProxyInfo.strProxyPass = strProxyPass;
    }



}
